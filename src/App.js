import React from 'react';
import MainLayout from './components/layout/MainLayout';
import MaterialUI from './components/MaterialUI';

function App() {
  return (
    <div>
      <MainLayout />
    </div>
  );
}

export default App;
