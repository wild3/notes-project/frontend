import React from 'react';
import MaterialAppBar from "../MaterialAppBar";

export default function MainLayout() {
    return (
        <>
            <MaterialAppBar />
        </>
    );
}